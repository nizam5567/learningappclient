export const settings = {
    pointsForEachQue: 10,
    passPercentage: 33,
    improvementMarginPercentage: 70
}