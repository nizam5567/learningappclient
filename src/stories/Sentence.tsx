import { gql, useQuery } from "@apollo/client";
import { MDBCard, MDBCardBody, MDBCardHeader, MDBContainer } from "mdbreact";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import BottomLinks from "../common/BottomLinks";

const Sentence = (props: any) => {
    let storyId = 0;
    if (props.match.params.storyId) {
        storyId = props.match.params.storyId;
    }

    const sentenceList = [
        { id: 1, contentId: 1, sentence: "Sample sentence 1", sentenceBangla: "পড়ুন" },
        { id: 2, contentId: 1, sentence: "Sample sentence 2", sentenceBangla: "শিখুন" },
        { id: 3, contentId: 1, sentence: "Sample sentence 3", sentenceBangla: "লিখুন" },
    ];

    const sentenceQuery = gql`
        query Query($storyId: String) {
            getSentencesByContentID(content_id: $storyId) {
                _id,
                sentenceText,
                sentenceTextBangla,                
            }
        }
    `;

    const { loading, error, data } = useQuery(sentenceQuery, { variables: { storyId } });
    
    const [sentenceData, setSentenceData] = useState<any>([]);

    useEffect(() => {
        if (!loading && data) {
            console.log("data", data);
            setSentenceData(data.getSentencesByContentID);
        }

    }, [loading, data]);  

    return (<>
        <MDBContainer>
            <MDBCard style={{ width: "100%", marginTop: "1rem" }}>
                <MDBCardHeader color="primary-color deep-orange lighten-1">Learn Sentence</MDBCardHeader>
                <MDBCardBody>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Sentence</th>
                                <th scope="col">Sentence in Bengali</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sentenceData && sentenceData.map((item: any, index: number) => {
                                return (<tr key={index}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{item.sentenceText}</td>
                                    <td>{item.sentenceTextBangla}</td>
                                </tr>);
                            })}


                        </tbody>
                    </table>

                </MDBCardBody>
            </MDBCard>

        </MDBContainer>
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div style={{ position: "absolute", bottom: "100px", left: 0, textAlign: "center", width: "100%" }}>
                        <Link to={"/storyQuestions/" + storyId}>
                            <button type="button" className="btn btn-secondary"
                                style={{ paddingLeft: "30px", paddingRight: "30px" }}>Start Exam</button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
        <BottomLinks />
    </>);
}

export default Sentence;