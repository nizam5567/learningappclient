// import React, { useState } from "react";
import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faHome } from '@fortawesome/free-solid-svg-icons';
import { faBookmark, faUser } from '@fortawesome/free-regular-svg-icons';
import BottomLinks from "../common/BottomLinks";
import { Modal, Button } from "react-bootstrap";
import { useHistory, useLocation } from "react-router-dom";
import { gql, useLazyQuery, useMutation, useQuery } from "@apollo/client";
import Loader from "react-loader-spinner";

export default function Favorite() {
  const tagData = [
    { id: 1, title: "Story", },
    { id: 2, title: "Lesson", },
  ];

  const storyCategoriesData = [
    { id: 1, title: "Upwork", readCount: 15, tileColor: "purple" },
    { id: 2, title: "Software Company", readCount: 20, tileColor: "red" },
    { id: 3, title: "Digital Agency", readCount: 10, tileColor: "orange" },
    { id: 4, title: "Sample Story", readCount: 12, tileColor: "green" },
    { id: 5, title: "Sample Story", readCount: 5, tileColor: "blue" },
    { id: 6, title: "Sample Story", readCount: 15, tileColor: "purple" },
  ];

  const favoriteItem = [
    { id: 1, contentId: 1, userId: 1 },
    { id: 2, contentId: 2, userId: 1 },
    { id: 3, contentId: 4, userId: 1 },
  ];

  const [modalContent, setModalContent] = useState<any>();

  const contents = gql`
    query Query {
      getContents {
        _id
        title
        tileColor
        shortDesc
        countUsersParticipate
      }
    }
  `;
  const { loading, error, data } = useQuery(contents);

  const favoritesQuery = gql`
        query Query {
            getFavorites {
                _id,
                user_id,
                content_id,
            }
        }
    `;
  // const { loading: favoritesLoading, error: favoritesError, data: favoritesData } = useQuery(favoritesQuery);
  const [getFavorites, { loading: favoritesLoading,
    error: favoritesError, data: favoritesData }] = useLazyQuery(favoritesQuery, {fetchPolicy: 'network-only'});

  useEffect(() => {
    getFavorites();
  }, []);

  useEffect(() => {
    if (!loading && data && !favoritesLoading && favoritesData) {
      let dataContents = [];
      for (let item of data.getContents) {
        //console.log(item);
        let obj = {
          id: item._id,
          title: item.title,
          tileColor: item.tileColor,
          shortDesc: item.shortDesc,
          countUsersParticipate: item.countUsersParticipate,
          isFavorite: false
        };

        if (favoritesData.getFavorites.find((favItem: any) => favItem.content_id === obj.id)) {
          obj.isFavorite = true;
          dataContents.push(obj);
        }

      }
      setFavoriteContent(dataContents);
      console.log("stories", dataContents);
    }

  }, [loading, data, favoritesLoading, favoritesData]);

  const location = useLocation();
  useEffect(() => {
    console.log('refresh');
    getFavorites();    
  }, [location.key])
  // const stories = storyCategoriesData.map((item: any) => {
  //   item.isFavorite = false;
  //   if (favoriteItem.find((favItem) => favItem.contentId === item.id)) {
  //     item.isFavorite = true;
  //   }
  //   return item;
  // });

  // const favoriteContent = stories.filter((item: any) => item.isFavorite)

  const [favoriteContent, setFavoriteContent] = useState<any>([]);

  const favoriteMutation = gql`
  mutation Mutation ($content_id: String!) {
    addOrRemoveFavorite(content_id: $content_id) {                
      user_id,
      content_id                
    }
  }
`;
  const [addOrRemoveFavorite, { data: favoriteData }] = useMutation(favoriteMutation);

  const [contentId, setContentId] = useState(0);
  const [showModal, setShowModal] = useState(false);

  const handleClose = () => setShowModal(false);
  const handleShow = (contentId: number) => {
    setContentId(contentId);
    const content = favoriteContent.filter((item: any) => { return item.id === contentId })[0];
    setModalContent(content);
    setShowModal(true);
  };

  const history = useHistory();
  const routeChangeToStory = () => {
    let path = "/story/" + contentId;
    history.push(path);
  }

  const handleFavorite = async (contentId: number) => {
    // const stories = favoriteContent.map((item: any) => {
    //   return item.id === contentId ? (item.isFavorite === false ? { ...item, isFavorite: true } : { ...item, isFavorite: false }) : item;
    // });
    const favContents = favoriteContent.filter((item: any) => {
      return item.id !== contentId;
    });

    setFavoriteContent(favContents);

    try {
      const { data } = await addOrRemoveFavorite({
        variables: {
          content_id: contentId
        }
      }) as any;
    } catch (err: any) {
      console.log("err", err.message);
    }

  };

  return (
    <div className="container">

      {(!loading && !favoritesLoading) && <div className="container bootstrap snippets bootdey appStoryContent">
        {/* <div className="row">
          <div className="col-md-12">
            <div className="userName">Favorite Story</div>
          </div>
        </div> */}
        <div className="row">
          <div className="col-md-12" style={{ margin: "15px 0 10px" }}>
            <h2><strong>Favorite Story</strong></h2>
          </div>
        </div>
        <div className="row">

          {favoriteContent.length ? favoriteContent.map((item: any, index: number) => {
            return (
              <div className="col-4" key={index} style={{ position: "relative" }}>

                <div onClick={() => handleShow(item.id)}>
                  <div className={"tile " + item.tileColor}>
                    <div className="title">{item.title}</div>
                    <p className="countParticipation">
                      {item.countUsersParticipate} {item.countUsersParticipate <= 1 ? "user" : "users"} users learned
                    </p>

                  </div>
                </div>

                <div className={"favoriteItem " + (item.isFavorite ? "favoriteItemActive" : "")}
                  onClick={() => handleFavorite(item.id)}>
                  <FontAwesomeIcon icon={faHeart} />
                </div>
              </div>
            );
          }) : <h1>Empty content</h1>}
        </div>

      </div>}

      {(loading || favoritesLoading) && <div className="spinnerCustom">
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
        // timeout={3000} //3 secs
        />
      </div>}

      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Story Details  - ({modalContent ? modalContent.title : ""})</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            {modalContent ? modalContent.shortDesc : ""}
          </div>
          <div style={{ margin: "10px 0", fontWeight: "bold" }}>
            Number of user participation: {modalContent ? modalContent.countUsersParticipate : 0}
          </div>
          <div style={{ margin: "10px 0", fontWeight: "bold" }}>Reting: 4.5</div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={routeChangeToStory}>
            Read Story
          </Button>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

    </div>
  );

}