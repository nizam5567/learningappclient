import { gql, useQuery } from "@apollo/client";
import { faHome, faBookmark, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { MDBContainer, MDBCard, MDBCardHeader, MDBCardBody, MDBCardText } from "mdbreact";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import BottomLinks from "../common/BottomLinks";
import FlipContent from "./FlipContent";

const StoryDetails = (props: any) => {
    let storyId = 0;
    if (props.match.params.id) {
        // storyId = parseInt(props.match.params.id);
        storyId = props.match.params.id.toString();
    }
    console.log("storyId", storyId);
    // const contentData = [
    //     { id: 1, title: "Sample story title 1", body: "Sample details 1", tagId: 1, storyCategoryId: 1 },
    //     { id: 2, title: "Sample story title 2", body: "Sample details 2", tagId: 1, storyCategoryId: 1 },
    //     { id: 3, title: "Sample story title 3", body: "Sample details 3", tagId: 1, storyCategoryId: 1 },
    // ];

    const contents = gql`
        query Query($storyId: String) {
            getContentByID(id: $storyId) {
                _id,
                title,
                tileColor,
                shortDesc,
                dialogs {
                    body,
                    bodyBangla
                }
            }
        }
    `;

    const { loading, error, data } = useQuery(contents, { variables: { storyId } });

    const dialogsData = [
        { id: 1, storyId: 1, content: "Programmer can build their career in Upwork.", contentBangla: "প্রোগ্রামার আপওয়ার্ক এ তাদের ক্যারিয়ার গড়তে পারে।" },
        { id: 2, storyId: 1, content: "Know the application process in Upwork.", contentBangla: "আপওয়ার্ক এ আবেদন প্রক্রিয়া জানুন।" },
        { id: 3, storyId: 1, content: "Step 1 - Create your Upwork profile.", contentBangla: "ধাপ 1 - আপনার আপওয়ার্ক প্রোফাইল তৈরি করুন।" },
        // { id: 4, storyId: 1, content: "dialog 4 dialog 4", contentBangla: "" },
        // { id: 5, storyId: 1, content: "dialog 5", contentBangla: "" },
        // { id: 6, storyId: 1, content: "dialog 6", contentBangla: "" }
    ];

    const dialogsDataByStoryId = dialogsData.filter((item: any) => {
        return item.storyId === storyId;
    });

    const [contentData, setContentData] = useState<any>([]);
    const [pagesFlipContent, setPagesFlipContent] = useState<any>([]);
    useEffect(() => {
        if (!loading && data) {
            console.log("data", data, data.getContentByID.dialogs);
            setContentData(data.getContentByID);

            let pages: any = [];
            let i = 0;

            const wordsPerPage = 100;
            pages[i] = "";
            data.getContentByID.dialogs.forEach((item: any, index: any) => {

                let dialogText = item.body + " #dialog# ";
                let currentTextEndPoint = 0;
                // console.log("--loop- ", Math.ceil(dialogText.length / wordsPerPage));
                for (let j = 0; j < Math.ceil(dialogText.length / wordsPerPage); j++) {
                    if (pages[i].length < wordsPerPage) {
                        let takePartLength = (wordsPerPage - pages[i].length);
                        pages[i] = pages[i] + dialogText.substr(0, takePartLength);
                        currentTextEndPoint = currentTextEndPoint + takePartLength;

                        if (Math.ceil(dialogText.length / wordsPerPage) === 1) {
                            let takePartLength = (wordsPerPage - pages[i].length);
                            pages[i] = pages[i] + dialogText.substr(currentTextEndPoint, takePartLength);
                            currentTextEndPoint = currentTextEndPoint + takePartLength;
                            if (currentTextEndPoint < dialogText.length) {
                                i++;
                                pages[i] = dialogText.substr(currentTextEndPoint, wordsPerPage);
                            }
                        }
                    } else {
                        i++;
                        pages[i] = dialogText.substr(currentTextEndPoint, wordsPerPage);
                        currentTextEndPoint = currentTextEndPoint + wordsPerPage;
                        if (currentTextEndPoint < dialogText.length &&
                            Math.ceil(dialogText.length / wordsPerPage) === (j + 1)) {
                            i++;
                            pages[i] = dialogText.substr(currentTextEndPoint, wordsPerPage);
                        }
                    }
                    // console.log('page length - ', i, pages[i].length);

                }
            });
            // console.log("pages", pages);

            for (let i = 1; i < pages.length; i++) {
                let index = pages[i].indexOf(' ');
                pages[i - 1] = pages[i - 1] + pages[i].substr(0, index);
                pages[i] = pages[i].substr(index, pages[i].length);
            }

            // console.log("new pages", pages);

            let pagesFlip: any[] = [];
            let counter = 0;
            loop1: for (let i = 0; i < pages.length; i++) {
                let pageDialogs = pages[i].split("#dialog#");

                // console.log("pageDialogs", pageDialogs);
                let pageDialogsData: any = [];
                
                loop2: for (let j = 0; j < pageDialogs.length; j++) {
                    
                    let bodyBangla = "";
                    if (pageDialogs.length > 1 && pageDialogs.length !== (j+1)) {
                        bodyBangla = data.getContentByID.dialogs[counter].bodyBangla;
                        counter++;
                    }
                    let obj = {
                        body: pageDialogs[j],
                        bodyBangla: bodyBangla
                    };
                    pageDialogsData.push(obj);
                }

                pagesFlip.push(pageDialogsData);


            }

            // console.log("pagesFlip", pagesFlip);
            setPagesFlipContent(pagesFlip);            
        }

    }, [loading, data]);

    const showNoOfDialog = 2;
    let startingIdx = 0;

    let count = 0;
    const visibleDialogData = dialogsData.filter((item, index) => {
        count++;
        return index >= startingIdx && count <= showNoOfDialog;
    });
    startingIdx = startingIdx + showNoOfDialog;

    const [visibleDialogs, setVisibleDialogs] = useState<any[]>(visibleDialogData);
    const [startingIndex, setStartingIndex] = useState(startingIdx);

    // const storyData = contentData.find(item => item.id === storyId);

    const getDialogData = (evt: any, isPreviousData: boolean = false) => {
        // console.log(startingIndex, isPreviousData);
        let count = 0;
        let startingIdx = startingIndex;
        if (isPreviousData) {
            startingIdx = startingIndex - (showNoOfDialog * 2);
            setStartingIndex(startingIdx);
        }

        const visibleDialogData = dialogsData.filter((item, index) => {
            if (index >= startingIdx) {
                count++;
            }
            return index >= startingIdx && count <= showNoOfDialog;
        });

        // console.log(visibleDialogData);
        setVisibleDialogs(visibleDialogData);
        const newStartingIndex = startingIdx + showNoOfDialog;
        setStartingIndex(newStartingIndex);
    }

    return (
        <div className="container" style={{ padding: 0 }}>
            {contentData && contentData.dialogs &&
                contentData.dialogs.length > 0 && pagesFlipContent && <FlipContent storyId={storyId}
                    title={contentData?.title}
                    // content={contentData.dialogs} 
                    content={pagesFlipContent} 
                    />}
        </div>
    );
}

export default StoryDetails;