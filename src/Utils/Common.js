// module for saving tokens to local storage
const TOKEN_KEY = "stampTokens";

// return the user data from the session storage
export const getUser = () => {
  const userStr = sessionStorage.getItem('user');
  if (userStr) return JSON.parse(userStr);
  else return null;
}

// return the token from the session storage
export const getToken = () => {
  // return sessionStorage.getItem('token') || null;
  return localStorage.getItem(TOKEN_KEY) || null;
}

// remove the token and user from the session storage
export const removeUserSession = () => {
  // sessionStorage.removeItem('token');
  // sessionStorage.removeItem('user');

  localStorage.removeItem(TOKEN_KEY);
}

// set the token and user from the session storage
export const setUserSession = (token, user) => {
  // sessionStorage.setItem('token', token);
  // sessionStorage.setItem('user', JSON.stringify(user));

  localStorage.setItem(TOKEN_KEY, JSON.stringify(token));
}