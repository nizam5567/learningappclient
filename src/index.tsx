import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { setContext } from '@apollo/client/link/context';

import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
  createHttpLink
} from "@apollo/client";
import { getToken, removeUserSession } from './Utils/Common';
import jwt_decode from "jwt-decode";
import history from './Utils/history';

const httpLink = createHttpLink({
  uri: 'http://localhost:4000/graphql',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = getToken();
  console.log("token", token);

  const tokenPart = token ? JSON.parse(token).split(' ')[1] : '';
  console.log("tokenPart", tokenPart);

  if (tokenPart !== '') {
    var decoded = jwt_decode(tokenPart) as any;
    // console.log(decoded);
    const expirationTime = (decoded.exp * 1000) - 60000;

    if (Date.now() >= expirationTime) {
      console.log("expire");
      //localStorage.clear();
      removeUserSession();
      history.push('/login');
    }
  }

  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      //authorization: token ? `Bearer ${token}` : "",
      authorization: token ? JSON.parse(token) : "",
    }
  }
});

const client = new ApolloClient({
  //uri: 'http://localhost:4000/graphql',
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>,
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
function jwtDecode(token: string | null): { exp: any; } {
  throw new Error('Function not implemented.');
}

